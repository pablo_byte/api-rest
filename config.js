require('dotenv').config();

module.exports = {
    //'uri_mongodb': 'mongodb://localhost:27017/inguz',
    'uri_mongodb': "mongodb+srv://pablo_byte:pablito30@cluster0-hoqys.mongodb.net/api_rest",
    'port': process.env.PORT,
    'expiresIn': parseInt(process.env.EXPIRESIN),
    'secret': process.env.SECRET,
};