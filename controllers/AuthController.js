var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

var User = require('../models/User');
let expiresIn = config.expiresIn
let secret = config.secret

exports.login_user = function (body) {
    const { email, password } = body;

    return new Promise(function (resolve, reject) {
        let resp = {
            status: false,
            message: "Error missing parameter"
        };
        User.findOne({
            email: email
        }).then(user => {
            resp = {
                status: false,
                message: 'Invalid User o Password'
            };

            if (!user)
                return reject(resp);

            let passwordIsValid = bcrypt.compareSync(password, user.password);
            if (!passwordIsValid) {
                resp.status = 401;
                resp.message = 'Usuario o Password inválida';
                return reject(resp);
            }

            //TOKEN
            const token = jwt.sign(
                { _id: user._id, rut: user.rut, rol: user.rol },
                secret,
                { expiresIn: expiresIn }
            )

            resp = {
                status: true,
                message: '',
                result: {
                    _id: user._id,
                    rut: user.rut,
                    firstname: user.firstname,
                    lastname: user.lastname,
                    email: user.email,
                    rol: user.rol,
                    status: user.status,
                    token: "Bearer " + token,
                }
            };
            return resolve(resp);
        })
            .catch(err => {
                console.log(err)
                return reject(err);
            });
    });

}
