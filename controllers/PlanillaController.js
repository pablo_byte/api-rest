var Planilla = require('../models/Planilla');
var UsuariosCalle = require('../models/UsuariosCalle');

exports.add_planilla = function (body) {
    let { institucion, ruta_social, region, comuna, horario, fecha, hora_ini, hora_fin,
        monitor, detalle } = body

    return new Promise(function (resolve, reject) {
        Planilla.create({
            "institucion": institucion,
            "ruta_social": ruta_social,
            "region": region,
            "comuna": comuna,
            "horario": horario,
            "fecha": fecha,
            "hora_ini": hora_ini,
            "hora_fin": hora_fin,
            "monitor": monitor,
            "detalle": detalle,
        }).then(
            planilla => {
                let resp = {
                    status: false,
                    message: 'Error'
                };

                if (!planilla) return reject(resp);

                resp = {
                    status: true,
                    message: 'Planilla added'
                };
                return resolve(resp);
            })
            .catch(err => {
                console.log(err.message);
                return reject({ status: false, message: err.message });
            });
    });

}

exports.add_usuarioPlanilla = function (_id, params) {
    //console.log(_id, params.id_usuario);
    return new Promise(function (resolve, reject) {
        let resp = {
            status: false,
            result: { message: 'Error' }
        };
        Planilla.findOneAndUpdate({ _id, 'detalle.id_usuario': { $ne: params.id_usuario } },
            { $push: { detalle: params } })
            .then(result => {
                //console.log(result);
                if (result !== null) {
                    resp = {
                        status: true,
                        result: { message: 'Usuario Calle added to Planilla' }
                    };
                } else {
                    resp = { status: false, result: { message: 'Usuario Calle not added to Planilla' } };
                }
                return resolve(resp);
            })
            .catch(err => {
                console.log('error', err);
                resp = {
                    status: false,
                    result: err.message
                };
                return reject(resp);
            });
    });
}

exports.del_usuarioPlanilla = function (_id, params) {
    //console.log(_id, params.id_usuario);
    return new Promise(function (resolve, reject) {
        let resp = {
            status: false,
            result: { message: 'Error' }
        };
        Planilla.findOneAndUpdate({ _id },
            { $pull: { 'detalle': { id_usuario: params.id_usuario } } })
            .then(result => {
                //console.log(result);
                if (result !== null) {
                    resp = {
                        status: true,
                        result: { message: 'Usuario Calle deleted from Planilla' }
                    };
                } else {
                    resp = { status: false, result: { message: 'Planilla not found/updated' } };
                }
                return resolve(resp);
            })
            .catch(err => {
                console.log('error', err);
                resp = {
                    status: false,
                    result: err.message
                };
                return reject(resp);
            });
    });
}

exports.get_all = function (params) {
    return new Promise(function (resolve, reject) {
        let resp = {
            status: false,
            result: { message: ' Error ' }
        };
        Planilla.find()
            .then(result => {
                if (result.length <= 0) {
                    resp.result.message = 'No hay registros para mostrar'
                    return reject(resp);
                }

                resp = { status: true, result: result };
                return resolve(resp);
            })
            .catch(err => {
                resp = {
                    status: false,
                    result: err
                };
                console.log(resp)
                return reject(resp);
            });
    });
}

exports.get_detallePlanilla = function (params) {
    return new Promise(function (resolve, reject) {
        let resp = {
            status: false,
            result: { message: 'Error' }
        };
        //console.log(params);
        Planilla.findById(params).populate('detalle.id_usuario')
            .then(result => {
                if (result.length <= 0) {
                    resp.result.message = 'Planilla not found';
                    return reject(resp);
                }

                resp = { status: true, result: result };
                return resolve(resp);
            })
            .catch(err => {
                resp = {
                    status: false,
                    result: err
                };
                console.log(resp)
                return reject(resp);
            });
    });
}

exports.delete = function (_id) {
    //console.log(params)
    return new Promise(function (resolve, reject) {
        let resp = {
            status: false,
            result: { message: ' Error ' }
        };
        Planilla.findByIdAndDelete(_id)
            .then(result => {
                //console.log(result);
                if (result !== null) {
                    resp = { status: true, result: { message: 'Planilla deleted' } };
                } else {
                    resp = { status: false, result: { message: 'Planilla not found' } };
                }
                return resolve(resp);
            })
            .catch(err => {
                //console.log('error', err)
                resp = {
                    status: false,
                    result: err.message
                };
                return reject(resp);
            });
    });
}

