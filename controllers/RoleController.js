var Profile = require('../models/Profile');
var Role = require('../models/Role');

exports.create_profile = function (body) {
    let { name, status } = body

    return new Promise(function (resolve, reject) {
        Profile.create({
                "name": name,
                "status": status || 'active'
            }).then(
                profile => {
                    let resp = {
                        status: false,
                        message: 'Error'
                    };
                
                    if (!profile) return reject(resp);

                    resp = {
                        status: true,
                        message: 'Profile created'
                    };
                    return resolve(resp);
            })
            .catch(err => {
                console.log(err)
                return reject(err);
            });
    });

}

exports.get_all_profiles = function (params) {
    return new Promise(function (resolve, reject) {
        let resp = {
            status: false,
            result: { message: ' Error ' }
        };
        Profile.find()
            .then(result => {
                if(result.length <= 0) {
                    resp.result.message = 'No hay registros a mostrar'
                    return reject(resp);
                }
                
                resp = { status: true, result: result };
                return resolve(resp);
            })
            .catch(err => {
                resp = {
                    status: false,
                    result: err
                };
                console.log(resp)
                return reject(resp);
            });
    });
}

exports.create_role = function (body) {
    let { name } = body

    return new Promise(function (resolve, reject) {
        Role.create({
                "name": name,
            }).then(
                role => {
                    let resp = {
                        status: false,
                        message: 'Error'
                    };
                
                    if (!role) return reject(resp);

                    resp = {
                        status: true,
                        message: 'Role created'
                    };
                    return resolve(resp);
            })
            .catch(err => {
                return reject(err);
            });
    });

}

exports.get_all_roles = function (params) {
    return new Promise(function (resolve, reject) {
        let resp = {
            status: false,
            result: { message: ' Error ' }
        };
        Role.find()
            .then(result => {
                if(result.length <= 0) {
                    resp.result.message = 'No hay registros a mostrar'
                    return reject(resp);
                }
                
                resp = { status: true, result: result };
                return resolve(resp);
            })
            .catch(err => {
                resp = {
                    status: false,
                    result: err
                };
                console.log(resp)
                return reject(resp);
            });
    });
}

/*--------*/
exports.get_all_users = function (params) {
    return new Promise(function (resolve, reject) {
        let resp = {
            status: false,
            result: { message: ' Error ' }
        };
        User.find({}, { password: 0 })
            .then(result => {
                if(result.length <= 0) {
                    resp.result.message = 'No hay registros a mostrar'
                    return reject(resp);
                }
                
                resp = { status: true, result: result };
                return resolve(resp);
            })
            .catch(err => {
                resp = {
                    status: false,
                    result: err
                };
                console.log(resp)
                return reject(resp);
            });
    });
}

exports.get_user = function (params) {
    return new Promise(function (resolve, reject) {
        let resp = {
            status: false,
            result: { message: ' Error ' }
        };
        User.findById(params, { _id: 0, password: 0 })
            .then(result => {
                if(result == null) {
                    return reject({ status: false, message: 'User not found' });
                }
                
                resp = { status: true, result: result };
                return resolve(resp);
            })
            .catch(err => {
                resp = {
                    status: false,
                    result: err
                };
                return reject(resp);
            });
    });
}

exports.update = function (userId, params) {
    //console.log(params)
    return new Promise(function (resolve, reject) {
        let resp = {
            status: false,
            result: { message: ' Error ' }
        };
        User.findByIdAndUpdate(userId, params, { _id: 0, password: 0 })
            .then(result => {                
                resp = { status: true, result: { message: 'User updated' } };
                return resolve(resp);
            })
            .catch(err => {
                console.log('error', err)
                resp = {
                    status: false,
                    result: err.message
                };
                return reject(resp);
            });
    });
}

exports.delete = function (userId) {
    //console.log(params)
    return new Promise(function (resolve, reject) {
        let resp = {
            status: false,
            result: { message: ' Error ' }
        };
        User.findByIdAndDelete(userId)
            .then(result => {                
                resp = { status: true, result: { message: 'User deleted' } };
                return resolve(resp);
            })
            .catch(err => {
                console.log('error', err)
                resp = {
                    status: false,
                    result: err.message
                };
                return reject(resp);
            });
    });
}

