var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

var User = require('../models/User');
let expiresIn = config.expiresIn
let secret = config.secret

function generatePassword(plainPassword) {
    return bcrypt.hashSync(plainPassword, 10);
}

function validatePassword(plainPassword, hashedPassword) {
    return bcrypt.compareSync(plainPassword, hashedPassword);
}

exports.create_user = function (body) {
    let { rut, firstname, lastname, email, password, rol, status } = body

    return new Promise(function (resolve, reject) {
        User.create({
                "rut": rut,
                "firstname": firstname,
                "lastname": lastname,
                "email": email,
                "password": password,
                "rol": rol || 'user',
                "status": status || 'active'
            }).then(
                user => {
                    let resp = {
                        status: false,
                        message: 'Error'
                    };
                
                    if (!user) return reject(resp);

                    let token = jwt.sign({
                        id: user._id,
                    }, secret, {
                        expiresIn: expiresIn
                    });

                    resp = {
                        status: true,
                        message: 'User register',
                        pass: user.password,
                        token: token
                    };
                    return resolve(resp);
            })
            .catch(err => {
                return reject(err);
            });
    });

}

exports.get_all_users = function (params) {
    return new Promise(function (resolve, reject) {
        let resp = {
            status: false,
            numrows: 0,
            result: { message: ' Error ' }
        };
        User.find({}, { password: 0 })
            .then(result => {
                if(result.length <= 0) {
                    resp.result.message = 'No hay registros a mostrar'
                    return reject(resp);
                }
                
                resp = { status: true, numrows: result.length, result: result };
                return resolve(resp);
            })
            .catch(err => {
                resp = {
                    status: false,
                    result: err
                };
                //console.log(resp)
                return reject(resp);
            });
    });
}

exports.get_user = function (params) {
    return new Promise(function (resolve, reject) {
        let resp = {
            status: false,
            result: { message: ' Error ' }
        };
        User.findById(params, { _id: 0, password: 0 })
            .then(result => {
                if(result == null) {
                    return reject({ status: false, message: 'User not found' });
                }
                
                resp = { status: true, result: result };
                return resolve(resp);
            })
            .catch(err => {
                resp = {
                    status: false,
                    result: err
                };
                return reject(resp);
            });
    });
}

exports.update = function (userId, params) {
    //console.log(params)
    return new Promise(function (resolve, reject) {
        let resp = {
            status: false,
            result: { message: ' Error ' }
        };
        User.findByIdAndUpdate(userId, params, { _id: 0, password: 0 })
            .then(result => {                
                resp = { status: true, result: { message: 'User updated' } };
                return resolve(resp);
            })
            .catch(err => {
                //console.log('error', err)
                resp = {
                    status: false,
                    result: err.message
                };
                return reject(resp);
            });
    });
}

exports.delete = function (userId) {
    //console.log(params)
    return new Promise(function (resolve, reject) {
        let resp = {
            status: false,
            result: { message: ' Error ' }
        };
        User.findByIdAndDelete(userId)
            .then(result => {                
                resp = { status: true, result: { message: 'User deleted' } };
                return resolve(resp);
            })
            .catch(err => {
                //console.log('error', err)
                resp = {
                    status: false,
                    result: err.message
                };
                return reject(resp);
            });
    });
}
