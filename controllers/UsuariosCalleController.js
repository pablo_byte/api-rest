var UsuariosCalle = require('../models/UsuariosCalle');

exports.add_usuarioCalle = function (body) {
    let { name, rut } = body

    return new Promise(function (resolve, reject) {
        UsuariosCalle.create({
            "name": name,
            "rut": rut || '1-9'
        }).then(
            response => {
                let resp = {
                    status: false,
                    message: 'Error'
                };

                if (!response) return reject(resp);

                resp = {
                    status: true,
                    message: 'Usuario Calle added'
                };
                return resolve(resp);
            })
            .catch(err => {
                console.log(err)
                return reject(err);
            });
    });

}

exports.add_puntoCalle = function (_id, params) {
    console.log(_id, params);
    return new Promise(function (resolve, reject) {
        let resp = {
            status: false,
            result: { message: ' Error ' }
        };
        UsuariosCalle.findByIdAndUpdate(_id, { $push: { puntos_calle: params } })
            .then(result => {
                resp = { status: true, result: { message: 'Punto inserted in Usuario Calle' } };
                return resolve(resp);
            })
            .catch(err => {
                //console.log('error', err)
                resp = {
                    status: false,
                    result: err.message
                };
                return reject(resp);
            });
    });
}

exports.get_all = function () {
    return new Promise(function (resolve, reject) {
        let resp = {
            status: false,
            result: { message: ' Error ' }
        };
        UsuariosCalle.find()
            .then(result => {
                if (result.length <= 0) {
                    resp.result.message = 'No hay registros a mostrar'
                    return reject(resp);
                }

                resp = { status: true, result: result };
                return resolve(resp);
            })
            .catch(err => {
                resp = {
                    status: false,
                    result: err
                };
                console.log(resp)
                return reject(resp);
            });
    });
}

exports.get_usuarioCalle = function (params) {
    return new Promise(function (resolve, reject) {
        let resp = {
            status: false,
            result: { message: ' Error ' }
        };
        UsuariosCalle.findById(params, { _id: 0, password: 0 })
            .then(result => {
                if (result == null) {
                    return reject({ status: false, message: 'Usuario Calle not found' });
                }

                resp = { status: true, result: result };
                return resolve(resp);
            })
            .catch(err => {
                resp = {
                    status: false,
                    result: err
                };
                return reject(resp);
            });
    });
}

exports.search_usuarioCalle = function (params) {
    console.log(params);
    return new Promise(function (resolve, reject) {
        let resp = {
            status: false,
            result: { message: ' Error ' }
        };
        UsuariosCalle.find({ name: { '$regex': params.query, '$options': 'i' } }, { _id: 0, password: 0 })
            .then(result => {
                if (result == null) {
                    return reject({ status: false, message: 'Usuario Calle not found' });
                }

                resp = { status: true, result: result };
                return resolve(resp);
            })
            .catch(err => {
                resp = {
                    status: false,
                    result: err
                };
                return reject(resp);
            });
    });
}

