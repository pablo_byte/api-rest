const mongoose = require('mongoose');

mongoose.connect(config.uri_mongodb,
    {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        useCreateIndex: true,
        useFindAndModify: false
    })
    .then(() => {
        console.log('Database connection successful');
    })
    .catch(err => {
        //console.log(err)
        console.error('Database connection error: ' + err.message);
    });
