var jwt = require('jsonwebtoken');
var moment = require('moment');
var config = require('./config');

exports.ensureAuthenticated = function (req, res, next) {
    if (!req.headers.authorization) {
        return res.send({ status: 403, message: "Tu petición no tiene cabecera de autorización" });
    }

    var token = req.headers.authorization.split(" ")[1];
    //console.log(token);
    var payload = jwt.decode(token, config.secret);
    //console.log(payload);

    jwt.verify(token, config.secret, function (err, decoded) {
        //console.log(decoded);
        if (err) {
            return res.status(500).send({ status:500, auth: false, message: 'Failed to authenticate token.' });
        } else {
            if (payload.exp <= moment().unix()) {
                return res.status(401).send({ status: 401, message: "El token ha expirado" });
            } else {
                req.user = payload.sub;
                next();
            }
        }

        //res.status(200).send(decoded);
    });

    
}
