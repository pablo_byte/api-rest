var mongoose = require('mongoose');

var PlanillasSchema = new mongoose.Schema({
    institucion: { type: String, trim: true, required: true, minlength: 3 },
    ruta_social: { type: String, required: true },
    region: { type: String, required: true },
    horario: { type: String, enum: ['AM', 'PM', 'Otro'], required: true },
    fecha: { type: Date, required: true },
    hora_ini: {
        type: Date,
        //required: true 
    },
    hora_fin: {
        type: Date,
        //required: true
    },
    monitor: { type: String, trim: true, required: true, minlength: 3 },
    detalle: [//new mongoose.Schema(
        {
            id_usuario: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'UsuariosCalle',
                required: true
            },
            punto_calle: { type: String, trim: true },
            prestaciones: [{
                type: String,
                required: true,
                enum: ['alimentacion', 'kit aseo', 'abrigo', 'traslado', 'otro']
            }],
            //usuario
        }, { _id: false }
        //)
    ]
}, {
    timestamps: true
});

PlanillasSchema.index(
    { "institucion": 1, "ruta_social": 1, "region": 1, "fecha": 1, "horario": 1 },
    { unique: true, dropDups: true }
);

//console.log(PlanillasSchema);
module.exports = mongoose.model('Planillas', PlanillasSchema);