const mongoose = require('mongoose');

var profileSchema = new mongoose.Schema({
    name: { type: String, unique: true, trim: true, required: true, minlength: 3 },
    status: {
        type: String, enum: ['active', 'inactive'], required: [true, 'Profile Status was missing']
    },
}, {
    timestamps: true,
});


module.exports = mongoose.model('Profile', profileSchema);