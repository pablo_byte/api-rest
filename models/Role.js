const mongoose = require('mongoose');

var roleSchema = new mongoose.Schema({
    name: { type: String, unique: true, trim: true, required: true, minlength: 3 },
    profile: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Profile'
    }
}, {
    timestamps: true,
});


module.exports = mongoose.model('Role', roleSchema);