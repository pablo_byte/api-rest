const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

var userSchema = new mongoose.Schema({
    rut: { type: String, unique: true, trim: true, required: true },
    firstname: { type: String, required: true, minlength: 3 },
    lastname: { type: String, required: true, minlength: 3 },
    email: {
        type: String, unique: true, required: true, minlength: 6,
        validate: value => {
            if (!validator.isEmail(value)) {
                throw new Error({ error: 'Invalid Email address' })
            }
        }
    },
    password: { type: String, required: true, minlength: 6 },
    rol: {
        type: String,
        enum: ['admin', 'user', 'seller', 'buyer'],
        required: [true, 'User Role was missing']
    },
    status: {
        type: String,
        enum: ['active', 'inactive'],
        required: [true, 'User Status was missing']
    },
}, {
    timestamps: true,
});

userSchema.pre('save', async function (next) {
    // Hash the password before saving the user model
    const user = this
    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 10)
    }
    next()
})

userSchema.pre('findOneAndUpdate', async function (next) {
    // Hash the password before updating the user model
    if (!this._update.password) {
        return next()
    }

    this.findOneAndUpdate({}, { password: await bcrypt.hash(this._update.password, 10) });
    next()
})

userSchema.methods.generateAuthToken = async function () {
    // Generate an auth token for the user
    const user = this
    const token = jwt.sign({ _id: user._id }, process.env.JWT_KEY)
    user.tokens = user.tokens.concat({ token })
    await user.save()
    return token
}

userSchema.statics.findByCredentials = async (email, password) => {
    // Search for a user by email and password.
    const user = await User.findOne({ email })
    if (!user) {
        throw new Error({ error: 'Invalid login credentials' })
    }
    const isPasswordMatch = await bcrypt.compare(password, user.password)
    if (!isPasswordMatch) {
        throw new Error({ error: 'Invalid login credentials' })
    }
    //console.log(user)
    return user
}

module.exports = mongoose.model('User', userSchema);