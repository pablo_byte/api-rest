const mongoose = require('mongoose');

var usuariosCalleSchema = new mongoose.Schema({
    name: { type: String, trim: true, required: true, minlength: 3 },
    rut: { type: String, trim: true, required: true, minlength: 3 },
    puntos_calle: [{
        punto_calle: { type: String, required: true },
        fecha: { type: Date, required: true },
    }, { _id: false }],
    verificado: { type: Boolean, default: false }
}, {
    timestamps: true,
});


module.exports = mongoose.model('UsuariosCalle', usuariosCalleSchema);