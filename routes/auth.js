'use strict'

var auth = require('../controllers/AuthController');

exports.login_user = async function (req, res, next) {
    let body = req.body;
    //console.log(body)
    try {
        let response = await auth.login_user(body);        
        res.status(200).send(response);

    } catch (err) {
        console.log(err);
        if(err.status === "" ) {
            res.status(400).send(err);
        } else {
            res.status(err.status).send(err);
        }
    }
   
}

exports.logout_user = async function (req, res, next) {
    let body = req.body;
    //console.log(body)
    try {
        res.status(200).send({ status: true, message: '', result: { token: null } });

    } catch (err) {
        //console.err
        res.status(400).send(err);
    }
   
}
