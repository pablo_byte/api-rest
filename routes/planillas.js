'use strict'

var planilla = require('../controllers/PlanillaController');

exports.create_planilla = async function (req, res, next) {
    let body = req.body;
    planilla.add_planilla(body)
        .then(response => {
            res.status(200).send(response);
        })
        .catch(err => {
            //console.log(err)
            res.status(400).send(err);
        })
}

exports.add_usuarioPlanilla = function (req, res, next) {
    let _id = req.params;
    let body = req.body;
    //console.log(params)
    planilla.add_usuarioPlanilla(_id, body)
        .then(response => {
            res.status(200).send(response);
        })
        .catch(err => {
            res.status(400).send(err);
        })
}

exports.del_usuarioPlanilla = function (req, res, next) {
    let _id = req.params;
    let body = req.body;
    planilla.del_usuarioPlanilla(_id, body)
        .then(response => {
            res.status(200).send(response);
        })
        .catch(err => {
            res.status(400).send(err);
        })
}

exports.get_all = function (req, res, next) {
    planilla.get_all()
        .then(response => {
            res.status(200).send(response);
        })
        .catch(err => {
            res.status(400).send(err);
        })
}

exports.get_detallePlanilla = function (req, res, next) {
    let _id = req.params;
    planilla.get_detallePlanilla(_id)
        .then(response => {
            res.status(200).send(response);
        })
        .catch(err => {

            res.status(400).send(err);
        })
}

exports.delete = function (req, res, next) {
    let _id = req.params;
    console.log(_id);
    planilla.delete(_id)
        .then(response => {
            res.status(200).send(response);
        })
        .catch(err => {
            res.status(400).send(err);
        })
}