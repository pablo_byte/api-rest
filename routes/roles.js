'use strict'

var role = require('../controllers/RoleController');

exports.create_profile = async function (req, res, next) {
    let body = req.body;

    role.create_profile(body)
    .then(response => {
        res.status(200).send(response);
    })
    .catch(err => {
        //console.log(err)
        res.status(400).send(err);
    })
}

exports.get_profiles = function (req, res, next) {
    role.get_all_profiles()
        .then(response => {
            res.status(200).send(response);
        })
        .catch(err => {

            res.status(400).send(err);
        })
}

exports.create_role = async function (req, res, next) {
    let body = req.body;

    role.create_role(body)
    .then(response => {
        res.status(200).send(response);
    })
    .catch(err => {
        console.log(err)
        res.status(400).send(err);
    })
}

exports.get_roles = function (req, res, next) {
    role.get_all_roles()
        .then(response => {
            res.status(200).send(response);
        })
        .catch(err => {

            res.status(400).send(err);
        })
}