'use strict'

var expressJoi = require('express-validation');
var validation = require('../tools/validation');
var middleware = require('../tools/middleware');

var auth = require('./auth');
var users = require('./users');
var roles = require('./roles');
var usuariosCalle = require('./usuarios-calle');
var planillas = require('./planillas');

var _ = require('lodash');

exports.assignRoutes = function assignRoutes(app) {
    // AUTH
    app.post('/api/v1/auth/login', expressJoi(validation.login), auth.login_user);
    app.post('/api/v1/users/logout', middleware.ensureAuthenticated, auth.logout_user);

    // USERS
    app.post('/api/v1/users/register', users.create_user);
    app.get('/api/v1/users/', middleware.ensureAuthenticated, users.get_all);
    app.get('/api/v1/users/:_id', middleware.ensureAuthenticated, users.get_user);
    app.patch('/api/v1/users/:_id', middleware.ensureAuthenticated, expressJoi(validation.update_user), users.update);
    app.delete('/api/v1/users/:_id', middleware.ensureAuthenticated, users.delete);


    // ROLES
    app.post('/api/v1/profiles', middleware.ensureAuthenticated, roles.create_profile);
    app.get('/api/v1/profiles/', middleware.ensureAuthenticated, roles.get_profiles);
    app.post('/api/v1/roles', middleware.ensureAuthenticated, roles.create_role);
    app.get('/api/v1/roles/', middleware.ensureAuthenticated, roles.get_roles);

    // USUARIOS CALLE
    app.post('/api/v1/usuario-calle/', middleware.ensureAuthenticated, usuariosCalle.add_usuarioCalle);
    app.patch('/api/v1/usuario-calle/:_id', middleware.ensureAuthenticated, usuariosCalle.add_puntoCalle);
    app.get('/api/v1/usuario-calle/', middleware.ensureAuthenticated, usuariosCalle.get_all);
    app.get('/api/v1/usuario-calle/:_id', middleware.ensureAuthenticated, usuariosCalle.get_usuarioCalle);
    app.get('/api/v1/usuario-calle/search/:query', middleware.ensureAuthenticated, usuariosCalle.search_usuarioCalle);

    // PLANILLAS
    app.post('/api/v1/planillas',  middleware.ensureAuthenticated,  /*expressJoi(validation.postPlanilla),*/ planillas.create_planilla);
    app.patch('/api/v1/planillas/:_id', middleware.ensureAuthenticated, planillas.add_usuarioPlanilla);
    app.delete('/api/v1/planillas/usuario/:_id', middleware.ensureAuthenticated, planillas.del_usuarioPlanilla);
    app.get('/api/v1/planillas',  middleware.ensureAuthenticated,  planillas.get_all);
    app.get('/api/v1/planillas/:_id',  middleware.ensureAuthenticated,  planillas.get_detallePlanilla);
    app.delete('/api/v1/planillas/:_id', middleware.ensureAuthenticated, planillas.delete);
    /*app.put('/api/trabajadores/:_id',  middleware.ensureAuthenticated, expressJoi(validation.putTrabajadores), trabajadores.modificar);
    app.get('/api/trabajadores/buscar/:_idEmpresa/:rol',  middleware.ensureAuthenticated, trabajadores.buscar_rol);
    app.get('/api/trabajadores/:_idEmpresa/:estado', middleware.ensureAuthenticated, expressJoi(validation.get_trabajadores), trabajadores.todos);
    app.delete('/api/trabajadores/:_id',  middleware.ensureAuthenticated, expressJoi(validation.delTrabajadores), trabajadores.eliminar);*/

    app.use(function (err, req, res, next) {
        if (err.errors) {
            if (err.errors.length > 0) {
                let messages = [];
                _.forEach(err.errors, function (value) {
                    _.forEach(value.messages, function (v) {
                        messages = _.concat(messages, v.replace(/"/g, "'"));
                    });
                });

                let resp = {
                    status: false,
                    message: messages
                };
                return res.status(400).send(resp);
            }
        }
    });

}
