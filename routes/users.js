'use strict'

var user = require('../controllers/UserController');

exports.create_user = async function (req, res, next) {
    let body = req.body;

    user.create_user(body)
    .then(response => {
        res.status(200).send(response);
    })
    .catch(err => {
        res.status(400).send(err);
    })
}

exports.get_all = function (req, res, next) {
    user.get_all_users()
        .then(response => {
            res.status(200).send(response);
        })
        .catch(err => {
            res.status(400).send(err);
        })
}

exports.get_user = function (req, res, next) {
    let userId = (req.params._id === 'me') ? req.userId: req.params;
    user.get_user(userId)
        .then(response => {
            res.status(200).send(response);
        })
        .catch(err => {
            res.status(400).send(err);
        })
}

exports.get_me = function (req, res, next) {
    user.get_me()
        .then(response => {
            res.status(200).send(response);
        })
        .catch(err => {
            res.status(400).send(err);
        })
}

exports.update = function (req, res, next) {
    let userId = req.params;
    let params = req.body;
    //console.log(params)
    user.update(userId, params)
        .then(response => {
            res.status(200).send(response);
        })
        .catch(err => {
            res.status(400).send(err);
        })
}

exports.delete = function (req, res, next) {
    let userId = req.params;
    user.delete(userId)
        .then(response => {
            res.status(200).send(response);
        })
        .catch(err => {
            res.status(400).send(err);
        })
}