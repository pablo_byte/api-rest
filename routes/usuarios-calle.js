'use strict'

var usuariosCalle = require('../controllers/UsuariosCalleController');

exports.add_usuarioCalle = async function (req, res, next) {
    let body = req.body;

    usuariosCalle.add_usuarioCalle(body)
        .then(response => {
            res.status(200).send(response);
        })
        .catch(err => {
            //console.log(err)
            res.status(400).send(err);
        })
}

exports.add_puntoCalle = async function (req, res, next) {
    let userId = req.params;
    let body = req.body;

    usuariosCalle.add_puntoCalle(userId, body)
        .then(response => {
            res.status(200).send(response);
        })
        .catch(err => {
            //console.log(err)
            res.status(400).send(err);
        })
}

exports.get_all = function (req, res, next) {
    usuariosCalle.get_all()
        .then(response => {
            res.status(200).send(response);
        })
        .catch(err => {

            res.status(400).send(err);
        })
}

exports.get_usuarioCalle = function (req, res, next) {
    let _id = req.params;
    usuariosCalle.get_usuarioCalle(_id)
        .then(response => {
            res.status(200).send(response);
        })
        .catch(err => {

            res.status(400).send(err);
        })
}

exports.search_usuarioCalle = function (req, res, next) {
    let query = req.params;
    usuariosCalle.search_usuarioCalle(query)
        .then(response => {
            res.status(200).send(response);
        })
        .catch(err => {

            res.status(400).send(err);
        })
}
