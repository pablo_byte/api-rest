var app = require('./app');
require('dotenv');

var port = process.env.PORT || config.port;
var server = app.listen(port, function() {
    console.log('Express server listening on port ' + port);
});