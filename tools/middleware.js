'use strict'

var jwt = require('jsonwebtoken');
var moment = require('moment');

var config = require('../config');
var secret = config.secret;

exports.ensureAuthenticated = function (req, res, next) {
    let resp = {
        status: false,
        message: ""
    };

    if (!req.headers.authorization) {
        resp.message = "Tu petición no tiene cabecera de Authorization";
        return res.status(401).send(resp);
    }

    var token = req.headers.authorization.split(" ")[1];
    //console.log('token', token)
    var payload = jwt.decode(token, secret);
    payload.time = payload.exp - moment().unix();
    //console.log('payload', payload)
    if (!payload) {
        resp.message = "Invalid token";
        return res.status(401).send(resp);
    }

    if (payload.exp <= moment().unix()) {// EXPIRED
        resp.message = "Expired token";
        return res.status(401).send(resp);
    }

    //console.log(payload)
    req.userId = { "_id": payload._id };
    req.role = payload.rol;

    next();
}

exports.ensureAuthenticatedMobile = function (req, res, next) {
    let resp = {
        status: false,
        message: ""
    };
    
    if (!req.headers['x-api-key']) {
        resp.message = "Tu petición no tiene cabecera X-API-KEY";
        return res.status(400).send(resp);
    }

    let api_key = req.headers['x-api-key'];
    let find = {
        rut: req.body.rut,
        _idEmpresa: req.body._idEmpresa
    }

}