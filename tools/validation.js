'use strict'

const Joi = require('joi');


exports.postPlanilla = {
    body: {
        rut: Joi.number().required(),
        dv: Joi.string().required(),
        _idEmpresa: Joi.number().required(),
        nombres: Joi.string().required(),
        apellidos: Joi.string().required(),
       // email: Joi.string().email(),
        nfc: Joi.string().required(),
        rol: Joi.string().required(),
        estado: Joi.boolean().required()
    }
}

exports.login = {
    body: {
        email: Joi.string().email().required(),
        password: Joi.string().required()
    }
}

exports.update_user = {
    body: {
        password: Joi.string().min(6),
    }
}

exports.get_trabajadores = {
    params: {
        _idEmpresa: Joi.string().required()
    }
}


exports.delTrabajadores = {
    params: {
        _id: Joi.string().required()
    }
}

exports.putTrabajadores = {
    params: {
        _id: Joi.string().required()
    },
    body: {
        nombres: Joi.string().required(),
        apellidos: Joi.string().required(),
        nfc: Joi.string().required(),
        rol: Joi.string().required(),
        estado: Joi.boolean().required()
    }
}